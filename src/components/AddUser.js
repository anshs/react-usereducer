import React, { useReducer, useState } from "react";
import { Modal } from "./Modal";
import { reducer } from "./Reducer";
import "../App.css";
import { UserList } from "./UserList";

const defaultState = {
  people: [],
  isModalOpen: false,
  modalContent: "",
};

export const AddUser = () => {
  const [name, setName] = useState("");
  const [state, dispatch] = useReducer(reducer, defaultState);

  const closeModal = () => {
    dispatch({ type: "CLOSE_MODAL" });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (name) {
      const newItem = { id: Math.floor(Math.random() * 100000000), name };
      dispatch({ type: "ADD_NAME", payload: newItem });
      setName("");
    } else {
      dispatch({ type: "NO_VALUE" });
    }
  };

  return (
    <>
      <form onSubmit={handleSubmit} className="form">
        {state.isModalOpen ? (
          <>
            <Modal closeModal={closeModal} modalContent={state.modalContent} />
          </>
        ) : (
          <div>
            <p>Enter name :-</p>
          </div>
        )}
        <div>
          <input
            type="text"
            value={name}
            onChange={(e) => {
              setName(e.target.value);
            }}
          />
        </div>
        <br />
        <button type="submit" className="btn btn-outline-success btn-sm">
          <b>Add</b>
        </button>
      </form>
      <br />
      <br />
      {state.people.map((person) => {
        return <UserList key={person.id} person={person} dispatch={dispatch} />;
      })}
    </>
  );
};
