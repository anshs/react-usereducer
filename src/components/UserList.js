import React from "react";

export const UserList = ({ person, dispatch }) => {
  return (
    <center>
      <div className="row peopleList">
        <div className="col">
          <h4>{person.name}</h4>
        </div>
        <div className="col divButton">
          <button
            onClick={() =>
              dispatch({ type: "REMOVE_NAME", payload: person.id })
            }
            className="btn btn-sm btn-outline-primary"
          >
            Remove
          </button>
        </div>
      </div>
    </center>
  );
};
