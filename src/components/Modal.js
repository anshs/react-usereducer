import React, { useEffect } from "react";
import "../App.css";

export const Modal = ({ closeModal, modalContent }) => {
  useEffect(() => {
    setTimeout(() => {
      closeModal();
    }, 3000);
  });
  return (
    <div className="messageModal">
      <p>{modalContent}</p>
    </div>
  );
};
